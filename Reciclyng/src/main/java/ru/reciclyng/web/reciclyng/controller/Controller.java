package ru.reciclyng.web.reciclyng.controller;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import ru.reciclyng.web.reciclyng.dao.UserDao;
import ru.reciclyng.web.reciclyng.forms.UserForm;
import ru.reciclyng.web.reciclyng.models.User;
import ru.reciclyng.web.reciclyng.repository.UserRepository;

import java.util.Collections;
import java.util.Optional;
import java.util.UUID;

@org.springframework.stereotype.Controller
public class Controller {
    @Autowired
    UserRepository userRepository;
    @Autowired
    private UserDao userDao;

    @GetMapping("/hello")
    public String helloPage(){
        return "helloPage";
    }

    @GetMapping("/signUp")
    public String signUp(){
        return "signUp";
    }

    @PostMapping("/signUp")
    public String addUser(UserForm userForm){
        User user = User.from(userForm);
        var checkLogin = userRepository.getUserList()
                .stream()
                .map(user1 -> user1.getLogin())
                .filter(login -> login.equals(user.getLogin()))
                .findFirst();
        if(checkLogin.isPresent())
            return "signUp";
        userDao.save(user);
        System.out.println(userRepository.getUserList().size());
        System.out.println(user);
        UUID id = user.getId();
        System.out.println(user);
        return "redirect:/user/"+id+"/profilePage";
    }

    @GetMapping("/user/{user_id}/profilePage")
    public String profilePage(@PathVariable("user_id")UUID userId, Model model){
        Optional<User> user = userDao.findById(userId);
        user.ifPresent(user1 -> System.out.println(user1));
        user.ifPresent(userOp -> model.addAttribute("user", userOp));
        return "profilePage";
    }

    @GetMapping("/logIn")
    public String logIn(UserForm userForm){
        var realUser = userRepository.getUserList()
                .stream()
                .filter(user -> user.getLogin().equals(userForm.getLogin()))
                .filter(user -> user.getPassword().equals(userForm.getPassword()))
                .findAny();
        if(realUser.isPresent()){
            return "redirect:/user/"+realUser.get().getId()+"/profilePage";
        }
        return "logInPage";
    }
}
