package ru.reciclyng.web.reciclyng.forms;

import lombok.Data;

@Data
public class UserForm {
    private String login;
    private String password;
}
