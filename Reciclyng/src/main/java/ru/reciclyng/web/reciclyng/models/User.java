package ru.reciclyng.web.reciclyng.models;

import lombok.Builder;
import lombok.Data;
import ru.reciclyng.web.reciclyng.forms.UserForm;

import java.util.UUID;

@Data
@Builder
public class User {

    private UUID id;
    private String login;
    private String password;

    public static User from(UserForm form) {
        return User.builder()
                .login(form.getLogin())
                .password(form.getPassword())
                .id(UUID.randomUUID())
                .build();
    }
}
