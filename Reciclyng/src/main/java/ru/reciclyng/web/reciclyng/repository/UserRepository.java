package ru.reciclyng.web.reciclyng.repository;

import lombok.Getter;
import org.springframework.stereotype.Component;
import ru.reciclyng.web.reciclyng.models.User;

import java.util.ArrayList;
import java.util.List;

@Component
@Getter
public class UserRepository {
     private static UserRepository instance;
     private List<User> userList = new ArrayList<>();

     public static UserRepository getInstance(){
         if(instance == null)
             instance = new UserRepository();
         return instance;
     }
}
