package ru.reciclyng.web.reciclyng;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class ReciclyngApplication {

    public static void main(String[] args) {
        SpringApplication.run(ReciclyngApplication.class, args);
    }

}
