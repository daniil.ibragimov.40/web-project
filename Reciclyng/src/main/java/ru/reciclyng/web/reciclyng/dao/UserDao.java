package ru.reciclyng.web.reciclyng.dao;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;
import ru.reciclyng.web.reciclyng.models.User;
import ru.reciclyng.web.reciclyng.repository.UserRepository;

import java.util.Optional;
import java.util.UUID;

@Component
public class UserDao {
    @Autowired
    private UserRepository userRepository;

    public UserDao(){
        userRepository = UserRepository.getInstance();
    }

    public void save(User user){
        userRepository.getUserList().add(user);
    }

    public void delete(User user){
        userRepository.getUserList().remove(user);
    }

    public boolean isUserExists(User user){
        return userRepository.getUserList().contains(user);
    }

    public Optional<User> findById(UUID id){
        return userRepository.getUserList().stream().filter(user -> user.getId().equals(id)).findFirst();
    }
}
